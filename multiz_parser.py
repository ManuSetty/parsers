#!/usr/bin/python

# File to parse the multiple sequence alignment file and 
# get alignments only for desired organisms
# The nucelotide in the reference will be picked if all except
# one of the align organisms have the same nucleotide

# Manu N Setty
# 11/11/2009

# Revised         Comments
# 11/16/2009      Added comments and fixed bug when no of organisms=2
# 04/07/2010      Bug fixes: Look for donservation in n-1 organisms,
#                            Lower or upper case in sequence does not matter
# 04/09/2010      Handling last block properly
# 12/12/2011      Rewrote code to make io inline and reduce memory footprint
# 17/02/2012      Bug fixes: Offset issue, alignment parsing for greater than 2 organisms

import sys
import commands
import string
import re
import math
import gzip

def usage():
    print "./multiz_parser.py MAF_FILE REF_ORG ALIGN_ORG1 ALIGN_ORG2...."
    print "MAF_FILE   : Aligned file downloaded from UCSC genome browser (in gz format)"
    print "REF_ORG    : String indicating the reference genome"
    print "ALIGN_ORGi : Other organisms to be aligned against"
    sys.exit ()


class Seq:
    """
    Seq Class
    """
    def __init__ (self, seqs, aligned_seq, line_length, outf):
        self.n = len (seqs)
        self.seqs = seqs
        self.aligned_seq = aligned_seq

        for pos in range(len(self.seqs[ref])):
            
            nucleotide = self.seqs[ref][pos]
            if nucleotide == "-":
                continue

            same_count = 0
            for org in seqs.keys():
                if nucleotide == self.seqs[org][pos]:
                    same_count = same_count + 1
            
            if (len(organisms)==2):
                if same_count==2:
                    self.aligned_seq = self.aligned_seq + nucleotide
                else:
                    self.aligned_seq = self.aligned_seq + "-"
            else:
                if same_count>=len (organisms)-1:
                    self.aligned_seq = self.aligned_seq + nucleotide
                else:
                    self.aligned_seq = self.aligned_seq + "-"
            if len (self.aligned_seq) == line_length:
                outf.writelines (self.aligned_seq + "\n")
                self.aligned_seq = ""
            


# Function for writing the fasta file
def write_fasta(sequence, line_length, outf, flush):
    counts= int (math.ceil (float (len (sequence))/line_length))
    outlines = [sequence[(i-1)*line_length:i*line_length] + "\n" for i in range (1, counts)]
    outf.writelines (outlines)

    if flush is True:
        outf.writelines (sequence[(counts-1)*line_length:len (sequence)] + "\n")
        return ""

    return sequence[(counts-1)*line_length:len (sequence)]


def main():
    global ref
    global organisms

    try:
        fname = sys.argv[1]
        ref = sys.argv[2]
        organisms = [ref]
        for i in range (3, len(sys.argv)):
            organisms.append (sys.argv[i])
    except:
        usage ()

    total_organisms = len (organisms)
    
    maf_file = gzip.GzipFile(fname, "r")
    p = re.compile ('[ ]+')
    outfname = fname.split(".")[0] + ".fa"
    outf = open (outfname, "w")
    outf.writelines (">" + outfname.split(".")[0] + "\n")
    line_length = 50
    line_counter = 0

    conserved_sequence = ''
    seq_counter = 0
    seqs = {}
    start = prev_end = end = 0
    dd = 0

    for line in maf_file:

        # Clean up line
        line = p.sub (' ', line.strip ())
        tokens = line.split (" ");

        # "a" indicates beginning of new alignment block
        if tokens[0]=="a":
            if len (seqs) >= total_organisms - 1:
                conserved_sequence = conserved_sequence + "-" * (start - prev_end)
                conserved_sequence = write_fasta(conserved_sequence, line_length, outf, False)
                conserved_sequence = Seq (seqs, conserved_sequence, line_length, outf).aligned_seq
            else:
                conserved_sequence = conserved_sequence + "-" * (end - prev_end)
                conserved_sequence = write_fasta(conserved_sequence, line_length, outf, False)
            seqs = {}
                        
        if tokens[0]!="s":
            continue
        
        # Keep loading the bases of different organisms
        if tokens[1].split(".")[0] in organisms:
            seqs[tokens[1].split(".")[0]] = tokens[6].upper ()

        # Mark the start and stop if the reference organisms is found
        if tokens[1].split(".")[0]==ref:
            prev_end = end
            start = int (tokens[2])
            end = start + int (tokens[3])
            chr_length = int (tokens[5])

    # Last block
    if len (seqs) >= total_organisms - 1:
       conserved_sequence = conserved_sequence + "-" * (start - prev_end - 1)
       conserved_sequence = write_fasta(conserved_sequence, line_length, outf, False)
       conserved_sequence = Seq (seqs, conserved_sequence, line_length, outf).aligned_seq
    else:
        conserved_sequence = conserved_sequence + "-" * (end - start - 1)
        conserved_sequence = write_fasta(conserved_sequence, line_length, outf, False)
                        
    if prev_end != chr_length:
        conserved_sequence = conserved_sequence + "-" * (chr_length - prev_end - 1)
    
    # Write 50 bases per line
    write_fasta(conserved_sequence, line_length, outf, True)
    maf_file.close ()
    outf.close ()

if __name__=="__main__":
   main()
