#!/usr/bin/python

## Script for parsing TargetScan Predicted Targets data
##  Manu N Setty
##  07/07/2011

# Revised         Comments
# 17/02/2012      Fixed to allow for inclusion of refseq ids in Targetscan downloads

import string
import sys

def usage():
    print './targetscan_predicted_targets_parser.py  PREDICTED_TARGETS_FILE ORG OUTPUT_FILE'
    print '   PREDICTED_TARGETS_FILE: Homologene data file'
    print '   ORG: Species ID to be used'
    print '   OUTPUT_FILE: Output file'
    sys.exit()


def addToHits (hits, gene, mir, score):

    try:
        h = hits[gene]
    except:
        hits[gene] = {}
        h = hits[gene]

    try:
        h[mir] = h[mir] + score
    except:
        h[mir] = score

    hits[gene] = h



def get_mir_hits (targets_file, org, out_file):
    
    # Read file
    print 'Reading file...'
    f = open (targets_file)
    lines = f.readlines ()
    f.close ()
    lines = map (string.strip, lines)
    # Ignore header line
    lines = lines[1:len (lines)]
    print 'Finished reading file...'

    hits = {}
    mirs = {}
    print 'Processing data (Determining unique hits)...'
    for line in lines:
        tokens = string.split (line, '\t')
        # Ignore other species
        if tokens[4] != org:
            continue

        mir = tokens[0]
        mirs[mir] = 1
        gene = tokens[2]
        addToHits (hits, gene, mir, 1)
    lines=''

    # Writing to file
    print 'Writing output...'
    outf = open (out_file, 'w')
    count = 0
    line = ''
    for mir in mirs.keys ():
        line = line + mir + '\t'
    line = line + '\n'
    outf.writelines (line)

    for gene in hits.keys ():
        line = gene + '\t'

        for mir in mirs.keys():
            try:
                line = line +  str (hits[gene][mir]) + '\t'
            except:
                line = line + '0\t'

        line = line + '\n'
        outf.writelines ([line])
        count = count + 1
        if count%10000 == 0:
            print count
    outf.close ()


def main ():
    try:
        targets_file = sys.argv[1]
        org = sys.argv[2]
        out_file = sys.argv[3]
    except:
        usage ()

    get_mir_hits (targets_file, org, out_file)


if __name__=='__main__':
    main ()

