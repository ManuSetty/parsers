#!/usr/bin/python

##  Script for determining exon junctions
##  Manu N Setty
##  08/18/2011

import sys
import string
import commands
import copy

def usage ():
    print 'create_splice_junctions GENES_FILE OUTPUT_BED RADIUS'
    print 'GENES_FILE               Refseq download'
    print 'OUTPUT_FILE               Name of the output file'
    print 'RADIUS                   Length to be included around splice junctions'
    sys.exit (-1)


def find_junctions (tokens, radius):
    # Parse line
    transcript = tokens[0]
    chr = tokens[1]
    strand = tokens[2]
    exon_starts = string.split (tokens[8], ',')
    exon_ends = string.split (tokens[9], ',')

    junctions = []

    # Find exon starts, ends and count
    exon_count = len (exon_starts) - 1
    for i in range (0, exon_count-1):
        j = [int (exon_ends[i])-radius, int (exon_ends[i]), int (exon_starts[i+1]), int (exon_starts[i+1])+radius]
        junctions.append (j)

    out_lines = ''
    for i in range (0, len (junctions)):
        line = ''
        for j in range (0, 4) :
            line = line + str (junctions[i][j])  + sep
        out_lines = out_lines + chr + sep + line +  \
            transcript + '|J' + str (i+1) + sep + strand + '\n'
    outf.writelines ([out_lines])


def main ():
    global sep
    global outf
    
    try:
        genesFile = sys.argv [1]
        outputFile = sys.argv [2]
        radius = int (sys.argv [3]) - 1
    except:
        usage ()

    sep = '\t'
    
    ## Read refseq file
    f = open (genesFile)
    lines = f.readlines ()
    f.close ()

    ## Open output file
    outf = open (outputFile, "w")
    for line in lines:
        tokens = string.split (line, '\t')
        find_junctions (tokens, radius)
    outf.close ()


if __name__=='__main__':
    main ()
