
## Script for converting hits output from different parsers
## to Rdata file

## Manu Setty
## 07/07/2011


text.to.rda <- function (hits.file, org) {

  ## Read file
  mirs.hits <- read.table (hits.file, header=TRUE, row.names=1, sep='\t')
  mirs.hits <- as.matrix (mirs.hits[,-ncol (mirs.hits)])

  ## Reset rownames
  include.eg.db (org)
  rownames (mirs.hits) <- convert.to.stdsymbols (rownames (mirs.hits))[rownames (mirs.hits)]

  ## Reset column names
  colnames (mirs.hits) <- scan (hits.file, 'character', nlines=1)

  save (mirs.hits, file=gsub ('txt', 'Rdata', hits.file))
}


      
  
