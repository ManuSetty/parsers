#!/usr/bin/python

##  Script for parsing through mirSVR predictions and create a matrix
##  Manu N Setty
##  03/16/2011

# Revised       Comments
# 03/31/2011    Using tab as separator instead of space

import sys
import string
import commands
import copy

def usage ():
    print 'mirSVR_parser.py PRED_FILE OUTPUT_FILE'
    print 'PRED_FILE                 mirSVR predictions'
    print 'FORMAT: Each row contains three columns which are tab-separated'
    print '        microRNA    gene     mirSVR-score'
    print 'NO HEADERS!!!!!'
    print 'OUTPUT_FILE               Output file'
    sys.exit (-1)

def addToScores (scores, gene, mir, location, score):

    obj = gene + '_' + mir + '_' + location
    try:
        if scores[obj] > score:
            scores[obj] = score
    except:
        scores[obj] = score
        

def addToHits (hits, gene, mir, score):

    try:
        h = hits[gene]
    except:
        hits[gene] = {}
        h = hits[gene]

    try:
        h[mir] = h[mir] + score
    except:
        h[mir] = score

    hits[gene] = h

def get_mir_hits (hitsFile, outputFile):
    
    # Read locations
    print 'Reading file.....'
    f = open (hitsFile)
    lines = map (string.strip, f.readlines ())
    f.close ()
    print 'Finished reading file...'

    hits = {}
    mirs = {}
    count = 0 
    print 'Processing data (Determining unique hits)...'
    for line in lines:
        tokens = string.split (line, '\t')
        mir = tokens[0]
        mirs[mir] = 1
        gene = tokens[1]
        score = float (tokens[2])
        addToHits (hits, gene, mir, score)
 
        # location = tokens[2]
        # score = float (tokens[3])
        # addToScores (scores, gene, mir, location, score)
        
        count = count + 1
        if count%100000 == 0:
            print count
    lines=''

    # print 'Processing data (Building matrix)...'
    # hits = {}
    # count = 0
    # for score in scores:
        # tokens = score.split ('_')
        # addToHits (hits, tokens[0], tokens[1], scores[score])
        # count = count + 1
        # if count%100000 == 0:
            # print count


    # Write output
    print 'Writing output...'
    outf = open (outputFile, 'w')
    count = 0
    line = ''
    for mir in mirs.keys ():
        line = line + mir + '\t'
    line = line + '\n'
    outf.writelines (line)

    for gene in hits.keys ():
        line = gene + '\t'

        for mir in mirs.keys():
            try:
                line = line +  str (hits[gene][mir]) + '\t'
            except:
                line = line + '0\t'

        line = line + '\n'
        outf.writelines ([line])
        count = count + 1
        if count%10000 == 0:
            print count
    outf.close ()


def main ():
    try:
        hitsFile = sys.argv [1]
        outputFile = sys.argv [2]
    except:
        usage ()

    get_mir_hits (hitsFile, outputFile)


if __name__=='__main__':
    main ()
