#!/usr/bin/python

## Script for parsing homologene data and determining mapping
##  Manu N Setty
##  07/06/2011

import string
import sys

def usage():
    print './homologene_parser.py  HOMOLOGENE_DATA_FILE ORG1 ORG2 OUTPUT_FILE'
    print '   HOMOLOGENE_DATA_FILE: Homologene data file'
    print '   ORG1: Taxonomy id of organism1'
    print '   ORG2: Taxonomy id of organism2'
    print '   OUTPUT_FILE: Output file'
    sys.exit()


def main ():
    try:
        homolo_file = sys.argv[1]
        org1 = sys.argv[2]
        org2 = sys.argv[3]
        out_file = sys.argv[4]

    except:
        usage ()
    
    # Read file
    f = open (homolo_file)
    lines = f.readlines ()
    f.close ()
    lines = map (string.strip, lines)

    all_mapping = []
    current_mapping = ['', '']
    current_ind = 'AA'
    for line in lines:
        words = string.split (line, '\t')
        # Adjust if index changes
        if words[0] != current_ind:
            current_ind = words[0]
            if current_mapping[0] != '' and current_mapping[1] != '':
                all_mapping.extend ([current_mapping])
            current_mapping = ['', '']
        # Look for organisms
        if words[1] == org1:
            current_mapping[0] = words[3]
        if words[1] == org2:
            current_mapping[1] = words[3]

    if current_mapping[0] != ''and current_mapping[1] != '':
        all_mapping.extend ([current_mapping])
    
    # Write output
    f = open(out_file, 'w')
    lines = []
    for maps in all_mapping:
        lines.extend (maps[0] + '\t' + maps[1] + '\n')
    f.writelines(lines)
    f.close()

if __name__=='__main__':
    main ()
